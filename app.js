const server = require('./server/express');

const run = async () => {
  await server();
};

run();
