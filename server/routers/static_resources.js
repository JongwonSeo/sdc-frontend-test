const path = require('path');
const express = require('express');
const router = express.Router();
const sass = require('node-sass-middleware');
const compression = require('compression');

router.use(compression());
router.use(
  sass({
    src: path.join(__dirname, '../../public'),
    dest: path.join(__dirname, '../../public'),
  }),
);

// js library
[
  '../../node_modules/chart.js/dist',
  '../../node_modules/popper.js/dist/umd',
  '../../node_modules/bootstrap/dist/js',
  '../../node_modules/jquery/dist',
].map((x) => {
  router.use('/js/lib', express.static(path.join(__dirname, x), {maxAge: 31557600000}));
});

router.use(
  '/webfonts',
  express.static(
    path.join(__dirname, '../../node_modules/@fortawesome/fontawesome-free/webfonts'),
    {
      maxAge: 31557600000,
    },
  ),
);

router.use('/', express.static(path.join(__dirname, '../../public'), {maxAge: 31557600000}));

module.exports = router;
