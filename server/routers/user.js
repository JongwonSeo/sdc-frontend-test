const path = require('path');
const express = require('express');
const router = express.Router();
const ejs = require('ejs');

const TAG = 'routers/user.js';
const VIEW_RELATED_PATH = '../../views';

const renderPage = (page, res) => {
  const shared = {
    proxy: '/',
    userSession: {
      privilege: 'user',
    },
  };
  ejs.renderFile(path.join(__dirname, VIEW_RELATED_PATH, page), shared, (err, body) => {
    if (!err) {
      res.render('index', {
        PAGE: body,
        ...shared,
      });
    } else {
      console.error(TAG, '👿 FAILED TO RENDER', err);
    }
  });
};

router.post('/signin', (req, res) => {
  // sign in process
  console.log(req.body.useremail, req.body.userpassword);
  const page = 'pages/index.ejs';
  renderPage(page, res);
});

router.get('/login', (req, res) => {
  const page = 'pages/login.ejs';
  renderPage(page, res);
});

router.get('/', (req, res) => {
  const page = 'pages/index.ejs';
  renderPage(page, res);
});

module.exports = router;
