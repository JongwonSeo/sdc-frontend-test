const express = require('express');

const bodyParser = require('body-parser');
const path = require('path');
const config = require('config');

const staticRouter = require('./routers/static_resources');
const router = require('./routers/user');

const TAG = 'app.js';
const app = express();

module.exports = () => {
  app.set('views', path.join(__dirname, '../views'));
  app.set('view engine', 'ejs');

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  app.use(staticRouter);
  app.use('/', router);

  app.listen(config.Port, function () {
    console.log(TAG, 'listening on port ' + config.Port);
  });
};
